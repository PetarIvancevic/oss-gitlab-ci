const _ = require('lodash')
const fs = require('fs')

function formatReadUserData (userData) {
  if (!userData) return

  // ako bude trebalo kasnije mozda dodavat vise podataka ili nesto
  const [handle] = userData.split(',')
  return { handle }
}

async function read (filename) {
  const readableStream = fs.createReadStream(`./${filename}`, { encoding: 'utf8' })

  let chunks = ''
  for await (const chunk of readableStream) {
    chunks += chunk
  }

  const userDataArray = _(chunks)
  .split('\n')
  .splice(1)
  .filter()
  .map(formatReadUserData)
  .value()

  return userDataArray
}

module.exports = {
  read,
}
