const got = require('got')

const CONSTANTS = require('consts')

const makeRequest = method => async (path, opts = {}) => {
  const url = `${CONSTANTS.GITLAB_API_URL}/${path}`
  const options = {
    ...opts,
    method,
    headers: {
      'Private-Token': process.env.GITLAB_TOKEN,
      'Content-Type': 'application/json',
    },
  }

  if (opts.body) {
    options.body = JSON.stringify(opts.body)
    options.responseType = 'json'
  }

  if (opts.query) {
    delete options.query
    options.searchParams = new URLSearchParams(opts.query).toString()
  }

  try {
    const resp = await got(url, options).json()
    return resp
  } catch (err) {
    console.error(err)
    throw err
  }
}

module.exports = {
  get: makeRequest('GET'),
  post: makeRequest('POST'),
}
