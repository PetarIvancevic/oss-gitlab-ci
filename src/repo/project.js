const api = require('src/utils/api')

async function create (userId, projectName) {
  return api.post('projects', {
    body: {
      user_id: userId,
      name: projectName,
      namespace_id: process.env.GROUP_ID,
    },
  })
}

async function addMemberToProject (projectId, userId) {
  return api.post(`/projects/${projectId}/members`, {
    body: {
      id: projectId,
      user_id: userId,
      access_level: 40, // maintainer access_level
    },
  })
}

module.exports = {
  addMemberToProject,
  create,
}
