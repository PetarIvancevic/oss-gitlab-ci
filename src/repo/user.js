const _ = require('lodash')
const api = require('src/utils/api')

async function getByUsername (username) {
  return api.get('users', {
    query: {
      username,
    },
  })
}

// need to be admin for this
async function inviteByEmail (user) {
  return api.post('users', {
    body: {
      email: user.handle,
      force_random_password: true,
      name: `${user.ime}-${user.prezime}`,
      private_profile: true,
      reset_password: true,
      organization: 'asinkrono-programiranje',
      username: _(user.handle).split('@').first(),
    },
  })
}

module.exports = {
  inviteByEmail,
  getByUsername,
}
