const _ = require('lodash')
const Promise = require('bluebird')

const userRepo = require('src/repo/user')
const projectRepo = require('src/repo/project')

async function email (user) {
  // not supported yet
}

async function gitlabHandle (user) {
  console.log('Inviting...', user)

  // get user gitlab id
  const gitlabUserId = _.get(_.first(await userRepo.getByUsername(user.handle)), 'id')

  if (!gitlabUserId) {
    throw new Error('Korisnik ne postoji!')
  }

  // create project for user
  const newProject = await projectRepo.create(gitlabUserId, `${user.handle}-vjezbe`)

  if (!_.get(newProject, 'id')) {
    throw new Error('Projekt nije moguce napraviti!')
  }

  // add user to project
  return projectRepo.addMemberToProject(newProject.id, gitlabUserId)
}

const wrapper = fn => userData => Promise.map(userData, async user => {
  try {
    await fn(user)
  } catch (err) {
    console.error('Failed to invite', user)
    console.error(err)
  }
})

module.exports = {
  email: wrapper(email),
  gitlabHandle: wrapper(gitlabHandle),
}
