#!/usr/bin/env node
require('dotenv-safe').config()
const yargs = require('yargs')

const inviteRepo = require('src/service/invite')
const utilsFile = require('src/utils/file')

const inviteTypes = ['email', 'gitlab-handle']
const inviteDescription = `
  Define which type of invite would you like to do?
  The current options are: [${inviteTypes.join(', ')}]
`

function logAndDie (err) {
  console.error(err)
  process.exit(1)
}

const argv = yargs
.option('type', {
  alias: 't',
  describe: inviteDescription,
  choices: inviteTypes,
  type: 'string',
  demandOption: true,
})
.help('help')
.parse()

async function delegateScript (type) {
  const filename = `studenti-${type}.csv`

  const userData = await utilsFile.read(filename)

  const inviteFn = type === 'email' ? inviteRepo.email : inviteRepo.gitlabHandle

  await inviteFn(userData)
}

delegateScript(argv.type).catch(logAndDie)
